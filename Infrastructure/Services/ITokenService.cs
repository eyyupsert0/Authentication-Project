﻿
using Domain.Common.Dto;
using Domain.Configuration;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public interface ITokenService
    {
        TokenDto CreateToken(User user);

        ClientTokenDto CreateTokenByClient(Client client);
    }
}
